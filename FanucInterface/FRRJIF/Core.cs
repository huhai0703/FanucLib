using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class Core
    {
        public Core()
        {
        }

        public bool Connect(string HostName)
        {
            return true;
        }

        public bool Disconnect()
        {
            return true;
        }

        public DataTable get_DataTable()
        {
            return null;
        }

        public bool ReadSDO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteSDO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadSDI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteSDI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadRDO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteRDO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadRDI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteRDI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadSO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteSO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadSI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteSI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadUO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteUO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadUI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteUI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool get_ProtectAvailable()
        {
            return true;
        }

        public int get_ProtectTrialRemainDays()
        {
            return int.MaxValue;
        }

        public string get_ProtectStatus()
        {
            return "Free";
        }

        public int get_ProtectErrorNumber()
        {
            return 0;
        }

        public bool WriteGO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadGO(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool ReadGI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public bool WriteGI(int Index, ref Array Buffer, int Count)
        {
            return true;
        }

        public string get_ProtectPCID()
        {
            return "1234";
        }

        public bool ProtectSetPassword(string strPassword)
        {
            return true;
        }

        public int get_TimeOutValue()
        {
            return 3;
        }

        public void set_TimeOutValue(int timeout)
        {
        }

        public DataTable get_DataTable2()
        {
            return null;
        }

        public int get_mlngDataTableCount()
        {
            return 2;
        }

        public int get_PortNumber()
        {
            return 60008;
        }

        public void set_PortNumber(int port)
        {
        }

        public short get_ClientID()
        {
            return 1234;
        }

        public void set_ClientID(short client)
        {
        }

        public bool ClearAlarm(int vlngType = 0)
        {
            return true;
        }

        public bool get_IsConnected()
        {
            return true;
        }

        public int get_ObjectID()
        {
            return 1;
        }

        public string get_DebugMessage()
        {
            return "";
        }

        public string GetDebugMessages()
        {
            return "";
        }

        public int get_ConnectState()
        {
            return 1;
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool get_DebugLog()
        {
            return DebugLog;
        }

        public void set_DebugLog(bool isDebug)
        {
            DebugLog = isDebug;
        }

        internal static bool DebugLog;
    }
}